import { lazy } from 'react'
import { IRoute } from '@/typings/router'
import Basic from './modules/basic'
import MyPages from './modules/my-pages'

const router: Array<IRoute> = [
  // { path: '/', redirectTo: '/basic/dashboard' },
  ...Basic,
  ...MyPages,
  {
    path: '/error/404',
    title: '错误页面',
    name: 'Error',
    key: 'error',
    icon: 'FileUnknownOutlined',
    hidden: true,
    children: [
      {
        title: '404',
        path: '/error/404',
        name: '404',
        icon: 'FileTextOutlined',
        component: lazy(() => import(/* webpackChunkName: "404" */ '@/pages/error/404')),
      },
    ],
  },
]

export default router
