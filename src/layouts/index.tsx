import React, { FC } from 'react'
import { Layout } from 'antd'
import { RouteComponentProps } from 'react-router'
import AppSider from './Sider'
import AppHeader from './Header'
import AppContent from './Content'

import './index.less'

const AppLayout: FC<RouteComponentProps> = () => {
  return (
    <Layout>
      <AppSider />
      <Layout style={{ minHeight: '100vh' }}>
        <AppHeader />
        <AppContent />
      </Layout>
    </Layout>
  )
}

export default AppLayout
