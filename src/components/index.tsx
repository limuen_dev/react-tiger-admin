import React, { FC } from 'react'
import { useStore } from '@/store'
import { observer } from 'mobx-react'
import Icon from '@/components/Icon'
import './index.scss'

const Hamburger: FC = () => {
  const { appStore } = useStore()

  const toggleSiderBar = () => {
    appStore.setCollapsed(!appStore.collapsed)
  }

  return (
    <div className="app-hamburger" onClick={toggleSiderBar}>
      { appStore.collapsed ? (<Icon type="MenuUnfoldOutlined" />) : (<Icon type="MenuFoldOutlined" />) }
    </div>
  )
}

export default observer(Hamburger)
