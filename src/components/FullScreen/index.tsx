import React, { useEffect, useState } from 'react'
import screenfull from 'screenfull'
import { message, Tooltip } from 'antd'
import { FullscreenOutlined, FullscreenExitOutlined } from '@ant-design/icons'
import './index.css'

const handleClick = () => {
  if (!screenfull.isEnabled) {
    message.warning('you browser can not work')
    return false
  }
  return screenfull.toggle()
}

const FullScreen = () => {
  const [isFullscreen, setIsFullscreen] = useState(false)

  const onChange = () => {
    setIsFullscreen(screenfull.isFullscreen)
  }

  useEffect(() => {
    screenfull.isEnabled && screenfull.on('change', onChange)
    return () => {
      screenfull.isEnabled && screenfull.off('change', onChange)
    }
  }, [])

  const title = isFullscreen ? '退出全屏' : '全屏'

  return (
    <div className="app-fullscreen" onClick={handleClick}>
      <Tooltip title={title}>
        <div>{isFullscreen ? <FullscreenExitOutlined /> : <FullscreenOutlined />}</div>
      </Tooltip>
    </div>
  )
}

export default FullScreen
